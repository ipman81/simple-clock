//
//  ViewController.swift
//  SimpleClock
//
//  Created by Adrian Watzlawczyk on 25/05/15.
//  Copyright (c) 2015 Adrian Watzlawczyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        label.text = ""
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "clock", userInfo: nil, repeats: true)
        
    }
    
    func clock(){
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitSecond, fromDate: date)
        
        var hour = components.hour > 12 ? components.hour - 12 : components.hour
        hour = hour == 0 ? 12 : hour
        
        var hourString = hour > 9 ? "\(hour)" : "0\(hour)"
        var minutes = components.minute > 9 ? "\(components.minute)" : "0\(components.minute)"
        var seconds = components.second > 9 ? "\(components.second)" : "0\(components.second)"
        let am = components.hour > 12 ? "PM" : "AM"
        
        label.text = "\(hourString) : \(minutes) : \(seconds) \(am)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

